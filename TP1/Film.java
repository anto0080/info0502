public class Film extends Media {
    private String realisateur;
    private int annee;

    public Film() {
        super();
        this.realisateur = "";
        this.annee = 0;
    }

    public Film(String titre, String cote, int note, String realisateur, int annee) {
        super(titre, cote, note);
        this.realisateur = realisateur;
        this.annee = annee;
    }

    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public String toString() {
        return "Film [Titre=" + getTitre() + ", Cote=" + getCote() + ", Note=" + getNote() + ", Réalisateur=" + realisateur + ", Année=" + annee + "]";
    }
}
