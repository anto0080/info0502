public class Livre extends Media {
    private String auteur;
    private String isbn;

    public Livre() {
        super();
        this.auteur = "";
        this.isbn = "";
    }

    public Livre(String titre, String cote, int note, String auteur, String isbn) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.isbn = isbn;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Livre [Titre=" + getTitre() + ", Cote=" + getCote() + ", Note=" + getNote() + ", Auteur=" + auteur + ", ISBN=" + isbn + "]";
    }
}
