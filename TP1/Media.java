
public class Media {
    private String titre;
    private StringBuffer cote;
    private int note;
    private static String nomMediatheque;

    // Constructeur par défaut
    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    // Constructeur d'initialisation
    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    // Constructeur de copie
    public Media(Media autre) {
        this.titre = autre.titre;
        this.cote = new StringBuffer(autre.cote);
        this.note = autre.note;
    }

    // Getters et Setters pour les variables d'instance
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return new StringBuffer(cote); // Retourne une copie pour éviter la modification directe
    }

    public void setCote(String cote) {
        this.cote = new StringBuffer(cote);
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    // Méthodes de classe pour manipuler la variable statique
    public static String getNomMediatheque() {
        return nomMediatheque;
    }

    public static void setNomMediatheque(String nom) {
        nomMediatheque = nom;
    }

    @Override
    public String toString() {
        return "Media [Titre=" + titre + ", Cote=" + cote + ", Note=" + note + "]";
    }
}
