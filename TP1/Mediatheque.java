import java.util.Vector;

public class Mediatheque {
    private String nomProprietaire;
    private Vector<Media> listeMedias;

    public Mediatheque() {
        this.nomProprietaire = "";
        this.listeMedias = new Vector<>();
    }

    public Mediatheque(Mediatheque autre) {
        this.nomProprietaire = autre.nomProprietaire;
        this.listeMedias = new Vector<>(autre.listeMedias);
    }

    public void add(Media media) {
        listeMedias.add(media);
    }

    @Override
    public String toString() {
        return "Mediatheque [NomProprietaire=" + nomProprietaire + ", ListeMedias=" + listeMedias + "]";
    }
}
