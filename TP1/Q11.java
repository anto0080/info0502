public class TestMediatheque {
    public static void main(String[] args) {
        Mediatheque mediatheque = new Mediatheque();
        Livre livre = new Livre("2002", "ABC123", 10, "autor", "1234578");
        Film film = new Film("Filmnom", "XYZ789", 9, "realisateur", 2002);

        mediatheque.add(livre);
        mediatheque.add(film);

        System.out.println(mediatheque);
    }
}
