package edu.info0502.tp2;

public class JeuxEssai {
    public static void main(String[] args) {
        // Créer un paquet de cartes
        PaquetDeCartes paquet = new PaquetDeCartes();
        paquet.melanger();

        // Distribuer une main privée à deux joueurs
        MainDePoker main1 = new MainDePoker(paquet);
        MainDePoker main2 = new MainDePoker(paquet);

        // Ajouter les 5 cartes communes
        main1.ajouterCartesCommunes(paquet);
        main2.ajouterCartesCommunes(paquet);

        // Afficher les mains des joueurs
        System.out.println("Main 1 : " + main1);
        System.out.println("Main 2 : " + main2);

        // Comparer les deux mains
        MainDePoker gagnante = MainDePoker.comparer(main1, main2);
        if (gagnante != null) {
            System.out.println("La main gagnante est : " + gagnante);
        } else {
            System.out.println("Les deux mains sont égales.");
        }
    }
}
