package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.HashMap;


public class MainDePoker {
    private ArrayList<Carte> mainPrivee;       // Les 2 cartes privées du joueur
    private ArrayList<Carte> cartesCommunes;   // Les 5 cartes communes

    // Constructeur : Distribue 2 cartes privées au joueur
    public MainDePoker(PaquetDeCartes paquet) {
        mainPrivee = new ArrayList<>();
        cartesCommunes = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            mainPrivee.add(paquet.tirerCarte());
        }
    }

    // Ajout des 5 cartes communes
    public void ajouterCartesCommunes(PaquetDeCartes paquet) {
        for (int i = 0; i < 5; i++) {
            cartesCommunes.add(paquet.tirerCarte());
        }
    }

    // Retourne la combinaison complète des 7 cartes (2 privées + 5 communes)
    public ArrayList<Carte> obtenirMainComplete() {
        ArrayList<Carte> mainComplete = new ArrayList<>(mainPrivee);
        mainComplete.addAll(cartesCommunes);
        return mainComplete;
    }

    // Évaluer la meilleure combinaison parmi les 7 cartes
    public int calculerMeilleureCombinaison() {
        ArrayList<Carte> mainComplete = obtenirMainComplete();
        // Pour simplifier, cette méthode retourne un score basé sur les meilleures combinaisons
        if (aUneSuite(mainComplete)) return 5;   // Quinte (suite)
        if (aUnBrelan(mainComplete)) return 3;   // Brelan
        if (aUnePaire(mainComplete)) return 1;   // Paire
        return 0;                                // Aucune combinaison
    }

    // Méthode pour détecter une paire
    private boolean aUnePaire(ArrayList<Carte> cartes) {
        HashMap<String, Integer> valeurs = new HashMap<>();
        for (Carte carte : cartes) {
            valeurs.put(carte.getValeur(), valeurs.getOrDefault(carte.getValeur(), 0) + 1);
        }
        for (int count : valeurs.values()) {
            if (count == 2) {
                return true;
            }
        }
        return false;
    }

    // Méthode pour détecter un brelan
    private boolean aUnBrelan(ArrayList<Carte> cartes) {
        HashMap<String, Integer> valeurs = new HashMap<>();
        for (Carte carte : cartes) {
            valeurs.put(carte.getValeur(), valeurs.getOrDefault(carte.getValeur(), 0) + 1);
        }
        for (int count : valeurs.values()) {
            if (count == 3) {
                return true;
            }
        }
        return false;
    }

    // Méthode pour détecter une suite (quinte)
    private boolean aUneSuite(ArrayList<Carte> cartes) {
        ArrayList<Integer> valeurs = new ArrayList<>();
        for (Carte carte : cartes) {
            valeurs.add(convertirValeur(carte.getValeur()));
        }
        valeurs.sort(Integer::compareTo);

        for (int i = 0; i < valeurs.size() - 1; i++) {
            if (valeurs.get(i) + 1 != valeurs.get(i + 1)) {
                return false;
            }
        }
        return true;
    }

    // Convertit les valeurs de carte en entier pour comparaison
    private int convertirValeur(String valeur) {
        switch (valeur) {
            case "As": return 14;
            case "Roi": return 13;
            case "Dame": return 12;
            case "Valet": return 11;
            default: return Integer.parseInt(valeur);
        }
    }

    // Méthode statique pour comparer deux mains et déterminer la plus forte
    public static MainDePoker comparer(MainDePoker main1, MainDePoker main2) {
        int scoreMain1 = main1.calculerMeilleureCombinaison();
        int scoreMain2 = main2.calculerMeilleureCombinaison();

        if (scoreMain1 > scoreMain2) {
            return main1;
        } else if (scoreMain1 < scoreMain2) {
            return main2;
        } else {
            return null;  // Égalité
        }
    }

    @Override
    public String toString() {
        return "Main Privée : " + mainPrivee + ", Cartes Communes : " + cartesCommunes;
    }
}
