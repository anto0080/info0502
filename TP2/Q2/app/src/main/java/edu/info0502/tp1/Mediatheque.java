package edu.info0502.tp1;

import java.util.Vector;

public class Mediatheque {
    private String nomProprietaire;
    private Vector<Media> listeMedias;

    public Mediatheque() {
        this.nomProprietaire = "";
        this.listeMedias = new Vector<>();
    }

    public Mediatheque(Mediatheque autre) {
        this.nomProprietaire = autre.nomProprietaire;
        this.listeMedias = new Vector<>(autre.listeMedias);
    }

    public void add(Media media) {
        listeMedias.add(media);
    }

    @Override
    public String toString() {
        return "Mediatheque [NomProprietaire=" + nomProprietaire + ", ListeMedias=" + listeMedias + "]";
    }

    // Add the main method here
    public static void main(String[] args) {
        // This is a simple example to run the application
        Mediatheque mediatheque = new Mediatheque();
        Media film = new Film("Inception", "C123", 5, "Christopher Nolan", 2010);
        Media livre = new Livre("1984", "L456", 5, "George Orwell", "1234567890");

        mediatheque.add(film);
        mediatheque.add(livre);

        System.out.println(mediatheque);
    }
}
