package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;

public class PaquetDeCartes {
    private ArrayList<Carte> cartes;

    public PaquetDeCartes() {
        cartes = new ArrayList<>();
        String[] couleurs = {"Pique", "Coeur", "Carreau", "Trèfle"};
        String[] valeurs = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi", "As"};

        for (String couleur : couleurs) {
            for (String valeur : valeurs) {
                cartes.add(new Carte(valeur, couleur));
            }
        }
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        return cartes.remove(0);  // Tire la première carte du paquet
    }
}
