package poker;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ClientMain {
    private static final String SERVER_IP = "10.11.21.136";
    private static final int SERVER_PORT = 12345;

    public static void main(String[] args) {
        try (Socket socket = new Socket(SERVER_IP, SERVER_PORT);
             ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

            System.out.println("Connected to the server.");

            // Receive initial message from the server
            String serverMessage = (String) in.readObject();
            System.out.println(serverMessage);

            // Get nickname from the user
            Scanner scanner = new Scanner(System.in);
            String nickname = null;
            while (nickname == null || nickname.trim().isEmpty()) {
                System.out.print("Enter a valid nickname: ");
                nickname = scanner.nextLine();
            }
            System.out.println("Debug: User entered nickname: " + nickname);

            // Send the nickname to the server
            out.writeObject(nickname);
            out.flush();
            System.out.println("Debug: Nickname sent to server.");

            // Wait for server response
            while ((serverMessage = (String) in.readObject()) != null) {
                System.out.println(serverMessage);
                if (serverMessage.startsWith("Welcome")) break;

                // Handle invalid nicknames
                System.out.print("Enter a new nickname: ");
                nickname = scanner.nextLine();
                out.writeObject(nickname);
                out.flush();
            }

            // Start listening for game events and send commands
            new Thread(() -> {
                try {
                    while (true) {
                        String message = (String) in.readObject();
                        System.out.println(message);

                        if (message.equals("START")) {
                            System.out.println("Game is starting!");
                            // Add more logic here to handle game events
                        }
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }).start();

            // Allow the user to send commands
            while (true) {
                System.out.print("Enter a command (e.g., START): ");
                String command = scanner.nextLine();
                out.writeObject(command);
                out.flush();
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

