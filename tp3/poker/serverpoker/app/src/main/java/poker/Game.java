package poker;

import java.io.IOException;
import java.util.*;

public class Game {
    private List<Player> players;
    private Deck deck;
    private List<Card> communityCards;

    public Game(List<Player> players) {
        this.players = players;
        this.deck = new Deck();
        this.communityCards = new ArrayList<>();
    }

    public void start() {
        System.out.println("Game started!");
        deck.shuffle();

        // Deal two cards to each player
        Map<Player, List<Card>> playerHands = new HashMap<>();
        for (Player player : players) {
            List<Card> hand = deck.dealCards(2);
            playerHands.put(player, hand);
            sendToPlayer(player, "Your hand: " + hand);
        }

        // Deal community cards
        dealCommunityCards();

        // Evaluate hands and determine the winner
        Player winner = evaluateHands(playerHands);

        // Broadcast everyone's hands to all players
        broadcastPlayerHands(playerHands);

        // Announce the winner
        if (winner != null) {
            broadcast("Game Over! The winner is: " + winner.getNickname());
        } else {
            broadcast("Game Over! It's a tie.");
        }
    }

    private void dealCommunityCards() {
        // Flop
        communityCards.addAll(deck.dealCards(3));
        broadcast("Flop: " + communityCards);

        // Turn
        communityCards.addAll(deck.dealCards(1));
        broadcast("Turn: " + communityCards);

        // River
        communityCards.addAll(deck.dealCards(1));
        broadcast("River: " + communityCards);
    }

    private Player evaluateHands(Map<Player, List<Card>> playerHands) {
        Player winner = null;
        int highestScore = 0;

        for (Map.Entry<Player, List<Card>> entry : playerHands.entrySet()) {
            Player player = entry.getKey();
            List<Card> hand = entry.getValue();

            // Calculate hand score (placeholder logic: sum of ranks)
            int score = calculateHandScore(hand, communityCards);
            sendToPlayer(player, "Your hand score: " + score);

            if (score > highestScore) {
                highestScore = score;
                winner = player;
            } else if (score == highestScore) {
                winner = null; // Tie
            }
        }

        return winner;
    }

    private int calculateHandScore(List<Card> hand, List<Card> communityCards) {
        // Simplified scoring: Sum of ranks (Ace = 14, King = 13, etc.)
        List<Card> allCards = new ArrayList<>(hand);
        allCards.addAll(communityCards);
        int score = 0;
        for (Card card : allCards) {
            score += getCardValue(card);
        }
        return score;
    }

    private int getCardValue(Card card) {
        String rank = card.getRank();
        switch (rank) {
            case "2": return 2;
            case "3": return 3;
            case "4": return 4;
            case "5": return 5;
            case "6": return 6;
            case "7": return 7;
            case "8": return 8;
            case "9": return 9;
            case "10": return 10;
            case "Jack": return 11;
            case "Queen": return 12;
            case "King": return 13;
            case "Ace": return 14;
            default: return 0;
        }
    }

    private void broadcastPlayerHands(Map<Player, List<Card>> playerHands) {
        for (Map.Entry<Player, List<Card>> entry : playerHands.entrySet()) {
            Player player = entry.getKey();
            List<Card> hand = entry.getValue();
            broadcast(player.getNickname() + "'s hand: " + hand);
        }
    }

    private void sendToPlayer(Player player, String message) {
        try {
            player.getOutputStream().writeObject(message);
            player.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void broadcast(String message) {
        for (Player player : players) {
            sendToPlayer(player, message);
        }
    }
}

