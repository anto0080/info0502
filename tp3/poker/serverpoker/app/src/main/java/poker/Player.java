package poker;

import java.io.ObjectOutputStream;

public class Player {
    private String nickname;
    private ObjectOutputStream outputStream;

    public Player(String nickname, ObjectOutputStream outputStream) {
        this.nickname = nickname;
        this.outputStream = outputStream;
    }

    public String getNickname() {
        return nickname;
    }

    public ObjectOutputStream getOutputStream() {
        return outputStream;
    }
}

