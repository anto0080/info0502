package poker;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class PlayerHandler implements Runnable {
    private Socket socket;
    private List<Player> players;

    public PlayerHandler(Socket socket, List<Player> players) {
        this.socket = socket;
        this.players = players;
    }

    @Override
    public void run() {
        try (ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

            System.out.println("New client connected: " + socket.getInetAddress());

            // Send an initial message
            out.writeObject("Enter your nickname:");
            out.flush();

            String nickname = (String) in.readObject();
            synchronized (players) {
                while (isNicknameTaken(nickname)) {
                    out.writeObject("Nickname already taken. Enter a new nickname:");
                    out.flush();
                    nickname = (String) in.readObject();
                }
                players.add(new Player(nickname, out));
                out.writeObject("Welcome, " + nickname + "!");
                out.flush();
                System.out.println("Debug: Welcome message sent to " + nickname);
            }

            // Keep the connection open to handle commands
            while (true) {
                String command = (String) in.readObject();
                System.out.println("Received command from " + nickname + ": " + command);

                if (command.equalsIgnoreCase("START")) {
                    synchronized (players) {
                        ServerMain.startGame();
                    }
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean isNicknameTaken(String nickname) {
        for (Player player : players) {
            if (player.getNickname().equals(nickname)) {
                return true;
            }
        }
        return false;
    }
}

