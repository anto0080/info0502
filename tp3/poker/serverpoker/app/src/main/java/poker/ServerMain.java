package poker;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class ServerMain {
    private static final int PORT = 12345;
    private static List<Player> players = new ArrayList<>();

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server is running on port " + PORT);

            while (true) {
                var socket = serverSocket.accept();
                System.out.println("New client connected.");
                new Thread(new PlayerHandler(socket, players)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void startGame() {
        if (players.size() >= 2) {
            System.out.println("Game is starting!");

            // Broadcast the START message to all players
            broadcastMessage("START");

            // Initialize and start the game
            Game game = new Game(players);
            game.start();
        } else {
            System.out.println("Not enough players to start the game.");
            broadcastMessage("Not enough players to start the game.");
        }
    }

    private static void broadcastMessage(String message) {
        for (Player player : players) {
            try {
                player.getOutputStream().writeObject(message);
                player.getOutputStream().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

