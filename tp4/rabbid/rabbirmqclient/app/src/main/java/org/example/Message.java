package org.example;

import java.util.Map;

public class Message {
    public String action;
    public String username;
    public String topic;
    public Map<String, String> answers;

    public Message(String action, String username, String topic, Map<String, String> answers) {
        this.action = action;
        this.username = username;
        this.topic = topic;
        this.answers = answers;
    }
}

