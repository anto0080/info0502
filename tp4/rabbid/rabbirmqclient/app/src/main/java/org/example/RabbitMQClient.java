package org.example;

import com.rabbitmq.client.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Scanner;

public class RabbitMQClient {
    private static final String REQUEST_QUEUE = "qcm_queue"; // For sending requests to the server
    private static final String RESPONSE_QUEUE = "client_queue"; // For receiving responses from the server

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.21.136");
        factory.setUsername("test");
        factory.setPassword("test");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(RESPONSE_QUEUE, false, false, false, null);

            // Listen for responses from the server
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String response = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [x] Received response from server: " + response);
            };
            channel.basicConsume(RESPONSE_QUEUE, true, deliverCallback, consumerTag -> {});

            Scanner scanner = new Scanner(System.in);
            System.out.println("RabbitMQ Client is running. Choose an action:");

            while (true) {
                System.out.println("\nSelect an action:");
                System.out.println("1. Register User");
                System.out.println("2. Request QCM");
                System.out.println("3. Submit Answers");
                System.out.println("4. Exit");
                System.out.print("Enter your choice: ");

                int choice = scanner.nextInt();
                scanner.nextLine(); // Consume newline
                String message;

                switch (choice) {
                    case 1:
                        System.out.print("Enter username to register: ");
                        String username = scanner.nextLine();
                        message = MessageUtils.toJson(new Message("register_user", username, null, null));
                        channel.basicPublish("", REQUEST_QUEUE, null, message.getBytes(StandardCharsets.UTF_8));
                        System.out.println(" [x] Sent: " + message);
                        break;

                    case 2:
                        System.out.print("Enter QCM topic: ");
                        String topic = scanner.nextLine();
                        message = MessageUtils.toJson(new Message("request_qcm", null, topic, null));
                        channel.basicPublish("", REQUEST_QUEUE, null, message.getBytes(StandardCharsets.UTF_8));
                        System.out.println(" [x] Sent: " + message);
                        break;

                    case 3:
                        System.out.println("Enter your answers in the format: id=answer (e.g., 1=A language)");
                        System.out.println("Type 'done' when finished.");
                        Message submitMessage = new Message("submit_answers", null, null, new HashMap<>());
                        while (true) {
                            System.out.print("Answer: ");
                            String input = scanner.nextLine();
                            if ("done".equalsIgnoreCase(input)) break;
                            String[] parts = input.split("=");
                            if (parts.length == 2) {
                                submitMessage.answers.put(parts[0], parts[1]);
                            } else {
                                System.out.println("Invalid format. Try again.");
                            }
                        }
                        message = MessageUtils.toJson(submitMessage);
                        channel.basicPublish("", REQUEST_QUEUE, null, message.getBytes(StandardCharsets.UTF_8));
                        System.out.println(" [x] Sent: " + message);
                        break;

                    case 4:
                        System.out.println("Exiting client.");
                        return;

                    default:
                        System.out.println("Invalid choice. Try again.");
                }
            }
        }
    }
}

