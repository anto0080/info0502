package org.example;

import com.rabbitmq.client.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RabbitMQServer {
    private static final String REQUEST_QUEUE = "qcm_queue"; // For receiving client requests
    private static final String RESPONSE_QUEUE = "client_queue"; // For sending responses to clients
    private static final Set<String> registeredUsers = new HashSet<>();
    private static final Map<String, String> correctAnswers = new HashMap<>();

    static {
        correctAnswers.put("1", "A language");
        correctAnswers.put("2", "A build tool");
    }

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.21.136");
        factory.setUsername("test");
        factory.setPassword("test");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(REQUEST_QUEUE, false, false, false, null);
            channel.queueDeclare(RESPONSE_QUEUE, false, false, false, null);
            System.out.println(" [*] Waiting for messages...");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String messageString = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [x] Received: " + messageString);

                try {
                    Message message = MessageUtils.fromJson(messageString, Message.class);

                    if ("register_user".equals(message.action)) {
                        handleRegisterUser(message.username);
                    } else if ("request_qcm".equals(message.action)) {
                        handleRequestQCM(channel, message.topic);
                    } else if ("submit_answers".equals(message.action)) {
                        handleSubmitAnswers(channel, message.answers);
                    } else {
                        System.out.println("Unknown action received.");
                    }
                } catch (Exception e) {
                    System.out.println("Error parsing message: " + e.getMessage());
                }
            };

            channel.basicConsume(REQUEST_QUEUE, true, deliverCallback, consumerTag -> {});

            synchronized (RabbitMQServer.class) {
                RabbitMQServer.class.wait();
            }
        }
    }

    private static void handleRegisterUser(String username) {
        System.out.println("Registering user: " + username);
        if (registeredUsers.add(username)) {
            System.out.println("User registered successfully!");
        } else {
            System.out.println("User already registered.");
        }
    }

    private static void handleRequestQCM(Channel channel, String topic) throws Exception {
        System.out.println("Processing QCM request for topic: " + topic);

        String response = "{ \"action\": \"qcm_questions\", \"questions\": [" +
                "{ \"id\": \"1\", \"question\": \"What is Java?\", \"options\": [\"A language\", \"A car\"] }," +
                "{ \"id\": \"2\", \"question\": \"What is Gradle?\", \"options\": [\"A build tool\", \"A dessert\"] }" +
                "] }";

        channel.basicPublish("", RESPONSE_QUEUE, null, response.getBytes(StandardCharsets.UTF_8));
        System.out.println(" [x] Responded with QCM questions to client_queue");
    }

    private static void handleSubmitAnswers(Channel channel, Map<String, String> userAnswers) throws Exception {
        System.out.println("Processing submitted answers...");

        int score = 0;
        for (Map.Entry<String, String> entry : userAnswers.entrySet()) {
            if (correctAnswers.containsKey(entry.getKey()) &&
                    correctAnswers.get(entry.getKey()).equals(entry.getValue())) {
                score++;
            }
        }
        int total = correctAnswers.size();

        String response = "{ \"action\": \"qcm_correction\", \"score\": " + score + ", \"total\": " + total + " }";
        channel.basicPublish("", RESPONSE_QUEUE, null, response.getBytes(StandardCharsets.UTF_8));
        System.out.println(" [x] Responded with QCM correction to client_queue: " + response);
    }
}

