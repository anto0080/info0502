package org.example;

import org.eclipse.paho.client.mqttv3.*;
import com.google.gson.Gson;
import org.example.models.PokerMessage;

import java.util.Scanner;

public class MqttPublisher {
    private static final String BROKER = "tcp://10.11.21.136:1883"; // Server IP
    private static final String TOPIC_TABLE_CREATE = "table/create";
    private static final String TOPIC_TABLE_JOIN = "table/join";
    private static final String TOPIC_DEAL = "table/deal";
    private static final String TOPIC_WINNER = "table/winner";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Gson gson = new Gson();

        try {
            MqttClient client = new MqttClient(BROKER, MqttClient.generateClientId());
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            client.connect(options);
            System.out.println("\nPoker client connected.");

            System.out.print("Enter your name: ");
            String playerName = scanner.nextLine().trim();

            System.out.print("Do you want to create a new table (y/n)? ");
            String createTable = scanner.nextLine().trim();
            String tableName;

            if (createTable.equalsIgnoreCase("y")) {
                System.out.print("Enter the name of the new table: ");
                tableName = scanner.nextLine().trim();

                // Create the table
                PokerMessage createMessage = new PokerMessage("create", playerName, tableName);
                client.publish(TOPIC_TABLE_CREATE, new MqttMessage(gson.toJson(createMessage).getBytes()));
                System.out.println("Table " + tableName + " created.");
            } else {
                System.out.print("Enter the name of the table to join: ");
                tableName = scanner.nextLine().trim();
            }

            // Join the table
            PokerMessage joinMessage = new PokerMessage("join", playerName, tableName);
            client.publish(TOPIC_TABLE_JOIN, new MqttMessage(gson.toJson(joinMessage).getBytes()));
            System.out.println("Joined table " + tableName + ".");

            // Subscribe to updates for this table
            client.subscribe("table/state/" + tableName, (topic, message) -> {
                String payload = new String(message.getPayload());
                PokerMessage stateMessage = gson.fromJson(payload, PokerMessage.class);
                System.out.println("\n[Game Update] " + stateMessage.getContent());
            });

            while (true) {
                System.out.println("\n1. Request Card");
                System.out.println("2. Determine Winner");
                System.out.println("3. Exit");
                System.out.print("Enter your choice: ");

                String choiceInput = scanner.nextLine().trim();
                int choice;

                try {
                    choice = Integer.parseInt(choiceInput);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input. Please enter a number.");
                    continue;
                }

                switch (choice) {
                    case 1:
                        PokerMessage dealMessage = new PokerMessage("deal", playerName, tableName);
                        client.publish(TOPIC_DEAL, new MqttMessage(gson.toJson(dealMessage).getBytes()));
                        System.out.println("Requested a card.");
                        break;

                    case 2:
                        PokerMessage winnerMessage = new PokerMessage("winner", playerName, tableName);
                        client.publish(TOPIC_WINNER, new MqttMessage(gson.toJson(winnerMessage).getBytes()));
                        System.out.println("Winner determination requested.");
                        break;

                    case 3:
                        client.disconnect();
                        System.out.println("\nDisconnected from the game.");
                        return;

                    default:
                        System.out.println("Invalid choice. Try again.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }
}

