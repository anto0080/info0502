package org.example;

import org.eclipse.paho.client.mqttv3.*;
import com.google.gson.Gson;
import org.example.models.PokerMessage;

import java.util.*;

public class MqttSubscriber {
    private static final String BROKER = "tcp://localhost:1883";
    private static final String TOPIC_TABLE_CREATE = "table/create";
    private static final String TOPIC_TABLE_JOIN = "table/join";
    private static final String TOPIC_DEAL = "table/deal";
    private static final String TOPIC_WINNER = "table/winner";
    private static final String TOPIC_STATE = "table/state";

    private static Map<String, Set<String>> tables = new HashMap<>(); // Table name to players
    private static Map<String, Map<String, List<Integer>>> playerCards = new HashMap<>(); // Table name -> Player -> Cards
    private static Gson gson = new Gson();
    private static Random random = new Random();

    public static void main(String[] args) {
        try {
            MqttClient server = new MqttClient(BROKER, MqttClient.generateClientId());
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            server.connect(options);
            System.out.println("Game server started. Waiting for players...");

            // Handle table creation
            server.subscribe(TOPIC_TABLE_CREATE, (topic, message) -> {
                String payload = new String(message.getPayload());
                PokerMessage pokerMessage = gson.fromJson(payload, PokerMessage.class);
                String tableName = pokerMessage.getContent();
                String playerName = pokerMessage.getPlayerName();

                if (!tables.containsKey(tableName)) {
                    tables.put(tableName, new HashSet<>());
                    playerCards.put(tableName, new HashMap<>());
                    System.out.println("Table created: " + tableName + " by " + playerName);
                    broadcastTableState(server, tableName);
                } else {
                    System.out.println("Table already exists: " + tableName);
                }
            });

            // Handle player joining a table
            server.subscribe(TOPIC_TABLE_JOIN, (topic, message) -> {
                String payload = new String(message.getPayload());
                PokerMessage pokerMessage = gson.fromJson(payload, PokerMessage.class);
                String tableName = pokerMessage.getContent();
                String playerName = pokerMessage.getPlayerName();

                if (tables.containsKey(tableName)) {
                    tables.get(tableName).add(playerName);
                    playerCards.get(tableName).put(playerName, new ArrayList<>()); // Initialize empty cards
                    System.out.println("Player " + playerName + " joined table: " + tableName);
                    broadcastTableState(server, tableName);
                } else {
                    System.out.println("Table does not exist: " + tableName);
                }
            });

            // Handle dealing cards
            server.subscribe(TOPIC_DEAL, (topic, message) -> {
                String payload = new String(message.getPayload());
                PokerMessage pokerMessage = gson.fromJson(payload, PokerMessage.class);
                String tableName = pokerMessage.getContent();
                String playerName = pokerMessage.getPlayerName();

                System.out.println("[DEBUG] Received DEAL request: Player=" + playerName + ", Table=" + tableName);

                if (playerCards.containsKey(tableName) && playerCards.get(tableName).containsKey(playerName)) {
                    int card = random.nextInt(13) + 1; // Random card between 1 and 13
                    playerCards.get(tableName).get(playerName).add(card);
                    System.out.println("[DEBUG] Dealt card " + card + " to player " + playerName + " at table " + tableName);

                    String state = "Player " + playerName + " received card " + card;
                    PokerMessage stateMessage = new PokerMessage("state", "server", state);
                    server.publish(TOPIC_STATE + "/" + tableName, new MqttMessage(gson.toJson(stateMessage).getBytes()));
                } else {
                    System.out.println("[DEBUG] Player " + playerName + " is not part of table " + tableName);
                }
            });

            // Handle determining the winner
            server.subscribe(TOPIC_WINNER, (topic, message) -> {
                String payload = new String(message.getPayload());
                PokerMessage pokerMessage = gson.fromJson(payload, PokerMessage.class);
                String tableName = pokerMessage.getContent();

                System.out.println("[DEBUG] Received WINNER request: Table=" + tableName);

                if (playerCards.containsKey(tableName)) {
                    String winner = determineWinner(playerCards.get(tableName));
                    String cardDetails = getCardDetails(playerCards.get(tableName));
                    String state = "The winner is " + winner + "!\nCards dealt:\n" + cardDetails;

                    PokerMessage winnerMessage = new PokerMessage("winner", "server", state);
                    server.publish(TOPIC_STATE + "/" + tableName, new MqttMessage(gson.toJson(winnerMessage).getBytes()));

                    System.out.println("[DEBUG] Winner determined: " + state);
                } else {
                    System.out.println("[DEBUG] No players found at table " + tableName);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void broadcastTableState(MqttClient server, String tableName) {
        try {
            String state = "Players at table " + tableName + ": " + tables.get(tableName);
            PokerMessage stateMessage = new PokerMessage("state", "server", state);
            String payload = gson.toJson(stateMessage);

            server.publish(TOPIC_STATE + "/" + tableName, new MqttMessage(payload.getBytes()));
            System.out.println("State broadcasted for table " + tableName + ": " + state);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String determineWinner(Map<String, List<Integer>> tablePlayers) {
        String winner = null;
        int maxScore = 0;

        for (Map.Entry<String, List<Integer>> entry : tablePlayers.entrySet()) {
            int score = entry.getValue().stream().mapToInt(Integer::intValue).sum();
            if (score > maxScore) {
                maxScore = score;
                winner = entry.getKey();
            }
        }
        return winner;
    }

    private static String getCardDetails(Map<String, List<Integer>> tablePlayers) {
        StringBuilder details = new StringBuilder();
        for (Map.Entry<String, List<Integer>> entry : tablePlayers.entrySet()) {
            details.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }
        return details.toString();
    }
}

