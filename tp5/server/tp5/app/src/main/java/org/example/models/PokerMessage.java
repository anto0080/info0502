package org.example.models;

public class PokerMessage {
    private String action;
    private String playerName;
    private String content;

    // Constructor
    public PokerMessage(String action, String playerName, String content) {
        this.action = action;
        this.playerName = playerName;
        this.content = content;
    }

    // Getters and Setters
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

